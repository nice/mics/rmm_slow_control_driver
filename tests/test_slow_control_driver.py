
import sys 
import json

from essrmmdriverlib import RMMRegisterAccess
from essrmmdriverlib import ReadoutMasterModule

def test_reg_reads():
    addr_map_json_path = "tests/test_addr_maps.json"
    print('Using Address Map JSON ' + addr_map_json_path)
    with open(addr_map_json_path,) as f:
        cfg_dict = json.load(f)
        addr_dict = cfg_dict["address_maps"]

    # construct new instance of RMMRegisterAccess
    RMMReg = RMMRegisterAccess.RMMRegisterAccess(addr_maps_dict=addr_dict)

    read_reg(RMMReg, "TEMP0")
    read_reg(RMMReg, "TEMP1")
    read_reg(RMMReg, "TEMP2")
    read_reg(RMMReg, "TEMP_HIGH")
    
def read_reg(RMMReg, name):
    success, data = RMMReg.regRead(name)
    if success:
        print("Successfully read " + str(data) + " from register " + name)
    else:
        print("Failed to read from register " + name)
    
    assert success

def test_rmm_temperature():
    """Test for RMM temperature conversion"""
    RMM = ReadoutMasterModule.ReadoutMasterModule()

    # Read temperature
    success, temp = RMM.getTemperature()
    if success:
        print("Successfully read temperature of " + str(temp) + " degrees C")
    else:
        print("Failed to read temperature")
    # Read Peak temperature
    success, temp = RMM.getPeakTemperature()
    assert success
    if success:
        print("Successfully read peak temperature of " + str(temp) + " degrees C")
    else:
        print("Failed to read peak temperature")
    assert success

    # Check for failing
    success, temp = RMM.getTemperatureFail()
    assert not success

def test_rmm_power_readout():
    """Test INA226 power monitor readout"""
    RMM = ReadoutMasterModule.ReadoutMasterModule()
    RMM.enablePowerMonitoring()
    success, results = RMM.getPowerVoltageCurrent()
    assert success
    print(str(results))
    #for sensor in results.items():
        # print(str(sensor))


if __name__ == "__main__":
    test_reg_reads()
    test_rmm_temperature()
    test_rmm_power_readout()