# README #

A python library to communicate with the ESS Readout Master Module (RMM)

## Scripts ##
"source create_venv.sh" to recreate the virtual environment
"source activate_venv.sh" to activate the virtual environment
Use "./build_lib.sh" to rebuild and reinstall the python library
Use "./run_tests.sh" to rebuild, reinstall, run the tests using pytest, and open the log file
Use "pytest" to run the tests without rebuilding

### Warning! ###
For the tests to execute successfully, an RMM must be connected to the machine running the tests, with a NIC set to the correct IP address/subnet to talk to the RMM. (Which is typically on 192.168.50.2)

## Installing ##

Install with pip:

    pip install dist/essrmmdriverlib-0.1.0-py3-none-any.whl