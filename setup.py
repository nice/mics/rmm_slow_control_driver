from setuptools import find_packages, setup

setup(
    name='essrmmdriverlib',
    packages=find_packages(include=['essrmmdriverlib']),
    version='0.2.0',
    description='Python Library to communicate with the ESS Readout Master Module',
    author='Joseph Hindmarsh',
    license='',
    python_requires='>=3.6',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    test_suite='tests'
)