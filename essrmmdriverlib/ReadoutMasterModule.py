
import os
import json

from essrmmdriverlib import RMMRegisterAccess
from essrmmdriverlib import RMMUtils

# temp global vars for quick packet counting
RX_PACKET_COUNT = 0
TX_PACKET_COUNT = 0


class ReadoutMasterModule:
    """A class for the ESS Readout Master Module to handle communcations
    
    Public Methods
    --------------

    getTemperature()
    getPeakTemperature()
    getTemperatureFail()        For testing, deliberately returns success=False
    
    """
    # Class Constants
   
    # Constructor
    def __init__(self, dest_ip='192.168.50.2', dest_udp=65535, cfg_json="tests/test_addr_maps.json", log_name="log.txt", log_mode='a'):
        """Creates a new ReadoutMasterModule Instance"""
        print('Creating New RMM instance')
        print('Using JSON Config ' + cfg_json)
        
        # handle JSON config file (load as dictionary)
        with open(cfg_json,) as f:
            self.cfg_dict = json.load(f)
        addr_dict = self.cfg_dict["address_maps"]
        
        # create new logger
        os.makedirs('log_files', exist_ok = True)
        self.dbg_log = RMMUtils.setupLogger(name="Register Access", log_fname="log_files/" + log_name, log_mode=log_mode)

        # Create a new RMMRegisterAccess instance
        self.RMMRegs = RMMRegisterAccess.RMMRegisterAccess(
            dest_ip=dest_ip,
            dest_udp=dest_udp,
            addr_maps_dict=addr_dict,
            existing_logger=self.dbg_log
        )

        self.power_monitors_calibrated = False
        self.power_monitors_enabled = False

    def getTemperature(self):
        """Gets the temperature of the RMM FPGA

        Args:
            None

        Returns:
            success (bool)
            temperature (float): Temperature in degrees C of the hottest temperature sensor
        """
        temperatures = {}
        list_of_temp_registers = ["TEMP0", "TEMP1", "TEMP2"]
        success = True # starting value
        current_temperature = -273.0
        for reg in list_of_temp_registers:
            success, raw_data = self.RMMRegs.regRead(reg, pre_success=success)
            temperatures[reg] = float(int(raw_data,16)/10.0)
            
            if temperatures[reg] > current_temperature:
                current_temperature = temperatures[reg]

        return success, current_temperature

    def getPeakTemperature(self):
        """Gets the peak temperature of the RMM FPGA

        Args:
            None

        Returns:
            success (bool)
            temperature (float): Temperature in degrees C of the hottest temperature sensor
        """
        success, raw_data = self.RMMRegs.regRead("TEMP_HIGH")
        temperature = float(int(raw_data,16)/10.0)
        return success, temperature

    def getTemperatureFail(self):
        """FOR TESTING: Fails to get temperature
        
        Returns:
            success (bool)
            temperature (float): Temperature in degrees C of the hottest temperature sensor
        """
        return False, -273.0

    def calibrateINA226s(self):
        """Calibrates INA226 voltage/current/power monitors on the VCU118
        
        Args:
            None
        
        Returns:
            success (bool)

        """
        success = self.RMMRegs.i2cClaimMutex()
        i2c_mux_addr = 0x75
        i2c_mux_position = 0x10
        success = self.RMMRegs.i2cWriteByte(i2c_mux_addr, i2c_mux_position, pre_success=success)
        success = self.RMMRegs.i2cWrite16bReg(0x40, 0x05, 0x0400, pre_success=success)   # INA0
        success = self.RMMRegs.i2cWrite16bReg(0x41, 0x05, 0x0400, pre_success=success)   # INA1
        success = self.RMMRegs.i2cWrite16bReg(0x42, 0x05, 0x0400, pre_success=success)   # INA2
        success = self.RMMRegs.i2cWrite16bReg(0x43, 0x05, 0x0400, pre_success=success)   # INA3
        success = self.RMMRegs.i2cWrite16bReg(0x44, 0x05, 0x1400, pre_success=success)   # INA4
        success = self.RMMRegs.i2cWrite16bReg(0x45, 0x05, 0x1400, pre_success=success)   # INA5
        success = self.RMMRegs.i2cWrite16bReg(0x48, 0x05, 0x0400, pre_success=success)   # INA6

        success = self.RMMRegs.i2cReleaseMutex(pre_success=success)
        self.power_monitors_calibrated = success
        return success
        
    def getPowerVoltageCurrent(self, voltage_tolerance_percent=1.0):
        """Gets the power, voltage and current for all INA226 sensors on the VCU118
        
        Args:
            voltage_tolerance_percent (float): percent tolerance of voltage from expected values (default = 1.0)
        
        Returns:
            success (bool)
            results (dict): Dictionary with the following entry for each sensor
                "$sensor": {
                    "V" : voltage in Volts (float)
                    "V_OK" : voltage within tolerance (bool)
                    "A" : current in Amperes (float)
                    "W" : power in Watts (float)
                }
                
        
        """
        success=True
        

        results_dict = {}
        # list of sensors
        sensors = ("VCCINT", "VCC1V8", "VADJ_1V8", "VCC1V2", "MGTAVCC","MGTAVTT", "VCC_INTIO_BRAM")
        # from https://docs.xilinx.com/v/u/en-US/ug1224-vcu118-eval-bd page 108
        expected_v = {
            "VCCINT": 0.85,
            "VCC1V8": 1.80,
            "VADJ_1V8": 1.80,
            "VCC1V2": 1.20,
            "MGTAVCC": 0.90,
            "MGTAVTT": 1.20,
            "VCC_INTIO_BRAM": 0.85
        }

        # create blank sub-dictionaries
        for sensor in sensors:
            results_dict[sensor] = {}

        # get the raw values
        final_success = True
        for num, sensor in enumerate(sensors):
            success, raw_power = self.RMMRegs.regRead("INP" + str(num), "C", pre_success=success)
            success, raw_voltage = self.RMMRegs.regRead("INV" + str(num), "C", pre_success=success)
            success, raw_current = self.RMMRegs.regRead("INA" + str(num), "C", pre_success=success)
          
            # Voltage measurements all have LSbits of 1.25mV
            v_lsb = 1.25e-3
            v_value = float(int(raw_voltage, 0)) * v_lsb
            results_dict[sensor]["V"] = v_value

            # convert to fractional
            voltage_tolerance_fractional=voltage_tolerance_percent/100
            print(str(voltage_tolerance_fractional))

            v_low_thresh = expected_v[sensor] * (1-voltage_tolerance_fractional)
            v_high_thresh = expected_v[sensor] * (1+voltage_tolerance_fractional)
            if (v_value >= v_low_thresh) and (v_value <= v_high_thresh):
                results_dict[sensor]["V_OK"] = True
            else:
                results_dict[sensor]["V_OK"] = False

            # Current measurements all have LSbits to 1mA, apart from VCCINT, which has a LSbit of 10mA
            if sensor == "VCCINT":
                i_lsb = 10e-3
            else:
                i_lsb = 1e-3
            results_dict[sensor]["A"] = float(int(raw_current, 0)) * i_lsb

            # Power measuremts have LSbits of 25 * i_lsb
            p_lsb = i_lsb * 25
            results_dict[sensor]["W"] = float(int(raw_power, 0)) * p_lsb

        return success, results_dict

    def enablePowerMonitoring(self, interval_ms=1000):
        """Enables the internal state machine to start reading out the Voltage,
        Current, and Power of various power rails on the VCU118
        
        Args:
            interval_ms (int): interval between updates

        Returns:
            succces (bool)
        """

        TICK_FREQ = 125e6   # 125MHz
        interval_ticks = float(interval_ms)/1000*TICK_FREQ

        while self.power_monitors_calibrated == False:
            success = self.calibrateINA226s()

        success = self.RMMRegs.regWrite("PWR_INTERVAL", int(interval_ticks), "C")
        success = self.RMMRegs.regWrite("PWR_ENABLE", 1, "C", pre_success=success)
        self.power_monitors_enabled = success
        return success

    def disablePowerMonitoring(self):
        """Disables the internal state machine that reads out the power rail information

        Args:
            None

        Returns:
            success (bool)
        """
        success = self.RMMRegs.regWrite("PWR_ENABLE", 0, "C")
        self.power_monitors_enabled = False
        return success


if __name__== "__main__":   
    pass

