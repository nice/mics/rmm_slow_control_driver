import os
import socket
import binascii
import time

from essrmmdriverlib import RMMUtils

class RMMRegisterAccess:
    """
    A class to access registers through the ESS Readout Master Module
    
    Attributes
    ----------
    dest_ip_addr : str
        Destination IP Address of the Readout Master Module (default="192.168.50.2")
    dest_udp_port : int
        Destination UDP Port of the Readout Master Module Slow Control (default=65535)
    addr_maps : dict
        Dictionary of address maps to map register names to raw addresses

    Public Methods
    --------------
    regWrite(name, data, success)
    regRead(name, success)
    rawWrite(addr, data)
    rawRead(addr)

    i2cClaimMutex()
    i2cReleaseMutex()
    i2cWriteByte(i2c_addr, data)
    i2cWrite8bReg(i2c_addr, reg_addr, data)
    i2cWrite16bReg(i2c_addr, reg_addr, data)
    i2cRead8bReg(i2c_addr, reg_addr)
    i2cRead8bRegSi570(i2c_addr, reg_addr)      For Si570 (which has a different command format)
    i2cRead16bReg(i2c_addr, reg_addr)
    i2cDelay(delay_us)



    Private Methods
    ---------------
    __init__(dest_ip, dest_udp, addr_maps_dict)
    __addrLookup(name, tag)
    __searchFileForAddr(file, name)
    __parseReplyRecvData(self, rec_data)
    __sendSlowControlPacket(pkt)

    

    """

    # Class Constants
    I2C_MUTEX_IDLE_ID = 0
    I2C_MUTEX_SCTL_ID = 1   # one-hot value (RMM microblaze is 2, RMM Power readout is 4)

    I2C_CMD_WRITE_ONE_BYTE = 0x01
    I2C_CMD_WRITE_TWO_BYTES = 0x02
    I2C_CMD_WRITE_THREE_BYTES = 0x03
    I2C_CMD_READ_BYTE = 0x04
    I2C_CMD_READ_TWO_BYTES = 0x05
    I2C_CMD_DELAY = 0x06
    I2C_CMD_READ_BYTE_SI570 = 0x07

    # Class Methods
    def __init__(self, dest_ip='192.168.50.2', dest_udp=65535, addr_maps_dict=None, existing_logger=None, log_name="log.txt", log_mode="a"):
        """Sets up the initial parameters of the RMMRegisterAccess class"""
        self.dest_ip_addr = dest_ip
        self.dest_udp_port = dest_udp
        self.buffer_size = 1024
        
        # Load dictionary of address map paths (not needed if we only want to use raw_read and raw_write)
        self.addr_maps_dict = addr_maps_dict

        self.tx_packet_count = 0
        self.rx_packet_count = 0

        # Allows use of a passed down logger
        if existing_logger == None:
            os.makedirs('log_files', exist_ok = True)
            self.dbg_log = RMMUtils.setupLogger(name="Register Access", log_fname="log_files/" + log_name, log_mode=log_mode)
        else:
            self.dbg_log = existing_logger

    def regWrite(self, reg_name, data, reg_tag=None, pre_success=True):
        """Writes 32bit data to a named register
        
        Args:
            reg_name (str): Name of register to write to
            data (str): Data to write (32b Hex string)
            reg_tag (str): Optional tag to tell us which address map to look in
            pre_success (bool): for chaining register accesses together and keeping track of the overall success
        Returns:
            success (bool) 

        """
        found, address = self.__addrLookup(reg_name, reg_tag)
        success = False
        if found:
            success = self.rawWrite(address, RMMUtils.hexFormat(data), reg_name)
        return pre_success and success

    def regRead(self, reg_name, reg_tag=None, pre_success=True):
        """Reads 32bit data from a named register
        
        Args:
            reg_name (str): Name of register to write to
            reg_tag (str): Optional tag to tell us which address map to look in
            pre_success (bool): for chaining register accesses together and keeping track of the overall success

        Returns:
            success (bool) 
            data (str): Read Data (32b Hex string)

        """
        found, address = self.__addrLookup(reg_name, reg_tag)
        success = False
        if found:
            success, data = self.rawRead(address, reg_name)
        return success and pre_success, data

    

    def __addrLookup(self, name, tag=None):
        """Looks up the address of a (named) register in one of the address map files
        
        Args:
            name (str): Name of register to lookup
            tag (str): File "tag" for quick reference to which address map in the address map dictionary (eg "M") 

        Returns:
            success (bool): True if found successfully
            addr (string): hex string of address from address map (without 0x prefix)
        """
        found = False
        if tag == None: # if no tag, check all files
            for file in self.addr_maps_dict.values():
                found, address = self.__searchFileForAddr(file, name)
                if found:
                    break
        else:   # if tag provided, only check that file
            if (tag in self.addr_maps_dict.keys()) == False:
                if tag == "M":
                    msg = "Master"
                elif tag == "S":
                    msg = "Slave"
                else:
                    msg = tag
                self.dbg_log.info("Error - please specify a register map for the " + msg)
                RMMUtils.handleError(message = "Error - please specify a register map for the " + msg, askUserForKeyPressBeforeQuitting = True)
            else:
                found, address = self.__searchFileForAddr(self.addr_maps_dict[tag], name)

        if (not found):
            if tag == None:
                self.dbg_log.error("Register " + name + " not found in files " + str(self.addr_maps_dict))
                RMMUtils.handleError(message = "Register " + name + " not found in files " + str(self.addr_maps_dict), askUserForKeyPressBeforeQuitting = True)
            else:
                self.dbg_log.error("Register " + name + " not found in file " + self.addr_maps_dict[tag])
                RMMUtils.handleError(message = "Register " + name + " not found in file " + self.addr_maps_dict[tag], askUserForKeyPressBeforeQuitting = True)
        return found, address

    def __searchFileForAddr(self, filepath, name):
        """Search an address map file for a named register
        
        Args:
            filepath (str): Path to the address map
            name (str): Name of the register we are looking up

        Returns:
            success (bool): True if found successfully
            addr (string): hex string of address from address map (without 0x prefix)

        """
        address = 0
        fp = RMMUtils.tryOpen(filepath, 'r')
        found = False
    
        for line in fp:
            word_arr = line.split(" ")
            if word_arr[0] == name:
                found = True
                address = word_arr[1].strip()
                break
        fp.close()

        # strip "0x" if present
        if address[0:2] == "0x":
            address = address[2:]
        
        return found, address


    def rawWrite(self, addr, data, reg_name=None):
        """Writes a (max 32b) hex string to a register address (also a 32b hex string)

        Args:
            addr (str): A 32b hex string of the register address (no 0x prefix)
            data (str): A 32b hex string of the data to write (no 0x prefix)
            reg_name (str): An optional register name to improve the logs
        
        Returns:
            success (bool)

        """
        write_head = "E55C0003"
        pkt = write_head + addr + data
        if reg_name == None:
            self.dbg_log.info("Writing 0x" + str(data) + " to address 0x" + str(addr))
        else:
            self.dbg_log.info("Writing 0x" + str(data) + " to 0x" + str(addr) +  " ( " + str(reg_name) + " )")

        success, rec_data = self.__sendSlowControlPacket(pkt)
                
        addr, data, status = self.__parseReplyRecvData(rec_data)

        # TODO: We could add more checks in here (ie do a register read), or those could be left to a higher level
        if status != "0x00000000":
            return False # auto-fail if there has been an error
        else:
            return success

    def rawRead(self, addr, reg_name=None):
        """Reads a (max 32b) hex string from a register address (also a 32b hex string)

        Args:
            addr (str): A 32b hex string of the register address
            reg_name (str): An optional register name to improve the logs
        
        Returns:
            success (bool)
            data (str): A 32b hex string of the return data

        """
        read_head = "E55C0001"
        pkt = read_head + addr
        if reg_name == None:
            self.dbg_log.info("Reading from address 0x" + str(addr))
        else:
            self.dbg_log.info("Reading from 0x" + str(addr) + " ( " + str(reg_name) + " )")
       
        success, rec_data = self.__sendSlowControlPacket(pkt)

        addr, data, status = self.__parseReplyRecvData(rec_data)    # this contains our status word parsing
        
        if reg_name == None:
            self.dbg_log.info("Read " + str(data) + " from address " + str(addr))
        else:
            self.dbg_log.info("Read " + str(data) + " from " + str(addr) + " ( " + str(reg_name) + " )")
        
        # TODO: We could add more checks in here (ie do a register read), or those could be left to a higher level
        if status != "0x00000000":  # auto-fail if there has been an error
            success = False
        
        return success, data

    def __sendSlowControlPacket(self, pkt, retry_limit=5, wait_for_input_on_retry=False):
        """Sends a packet to the RMM
        
        Args:
            pkt (hex-str): packet to send (hex string with no 0x prefix)
            retry_limit (int): number of timeouts before failure
            wait_for_input_on_retry (bool): wait for use input before retrying when we timeout

        Returns:
            success (bool)
            rec_data (hex-str): hex string of received data (no 0x prefix)
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.settimeout(5.0)        # should raise a timeout exception
        host = (self.dest_ip_addr, self.dest_udp_port)
    
        retry = True
        retry_count = 0
        while retry:
            try:
                sent = sock.sendto(binascii.unhexlify(pkt), host)
                self.tx_packet_count = self.tx_packet_count + 1
                rec_data, rec_host = sock.recvfrom(self.buffer_size)
                self.rx_packet_count = self.rx_packet_count + 1
                retry = False
            except socket.timeout:
                retry_count = retry_count + 1
                self.dbg_log.error("Packets Sent     : " + str(self.tx_packet_count))
                self.dbg_log.error("Packets Received : " + str(self.rx_packet_count))
                print("Slow Control Timeout! Retrying... (retry count = " + str(retry_count) + ")")
                if wait_for_input_on_retry:
                    input("> Waiting for permission to continue...")  
                if retry_count > retry_limit:
                    return False, 0
    
        return True, rec_data

    def __parseReplyRecvData(self, rec_data):
        """Parse a slow control reply packet data and handle the Status Word
        Returns a tuple of the data, address, and status word (all 32b hex strings with '0x' prefix"""

        ## Split the return packet (read/write both same format) into its fields
        # 8 hex chars of header, 8 hex chars of address, 8 hex chars of data, 8 hex chars of status
        #  python2 returns rec_data as str type ; python3 is bytes array :  
        header = ((rec_data[0:4]).hex()).upper()	  
        addr = ((rec_data[4:8]).hex()).upper()	  
        data = ((rec_data[8:12]).hex()).upper()	  
        status = ((rec_data[12:]).hex()).upper()	  

        # If present, Check STATUS word for errors
        if status != "":
            if status != "00000000":
            # handle_error(message = "ERROR: STATUS WORD Error Received, status=" + str(status), askUserForKeyPressBeforeQuitting = True)
                if status[0:4] != "0000":    # bits 27:16 of status word are our firewall blocked bits for rings 11:0 
                    firewalls_blocked = RMMUtils.which_bits_are_set(status[0:4])
                    for logical_ring in firewalls_blocked:
                        phys_ring = logical_ring * 2
                        self.dbg_log.info("    Status: AXI Firewall on logical ring " + str(logical_ring) + " BLOCKED. (Physical Rings " + str(phys_ring) + " and " + str(phys_ring+1) + ")")
                    RMMUtils.handleError(message = "    Status: AXI Firewall BLOCKED - check ILAs", askUserForKeyPressBeforeQuitting = True)
                if status[7] == "0":
                    self.dbg_log.info("    Status: AXI OKAY Response Received")
                elif status[7] == "1":    
                    RMMUtils.handleError(message = "    Status: AXI EXOKAY Response Received", askUserForKeyPressBeforeQuitting = True) # For now, ask User to manually quit thu' key press - later may want to audoQuit instead
                elif status[7] == "2":
                    RMMUtils.handleError(message = "    Status: AXI SLVERR Response Received : Register not found at that address - check address maps", askUserForKeyPressBeforeQuitting = True)
                elif status[7] == "3":
                    RMMUtils.handleError(message = "    Status: AXI DECERR Response Received", askUserForKeyPressBeforeQuitting = True)
                else:
                    RMMUtils.handleError(message = "    Status: UNDEFINED AXI Response Received", askUserForKeyPressBeforeQuitting = True)
            
        ## add the "0x" on again before we return the parsed fields
        header = "0x" + header
        addr = "0x" + addr
        data = "0x" + data
        status = "0x" + status
        
        return addr, data, status

    def regWriteFail(self, reg_name, data, reg_tag=None, pre_success=True):
        """FOR TESTING: Fail to write 32bit data to a named register 
        
        Args:
            reg_name (str): Name of register to write to
            data (str): Data to write (32b Hex string)
            reg_tag (str): Optional tag to tell us which address map to look in
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)
            
        Returns:
            success (bool) 

        """
        # found, address = self.__addrLookup(reg_name, reg_tag)
        return False

    def regReadFail(self, reg_name, reg_tag=None, pre_success=True):
        """FOR TESTING: Fail to read a named 32bit register
        
        Args:
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool) 
        """
        return False, "0xffffffff"

    def i2cClaimMutex(self, id=I2C_MUTEX_SCTL_ID, poll_delay=1.0, poll_attempts_timeout=None, pre_success=True):
        """Attempts to claim the RMM I2C mutex so that I2C transactions can be done safely
        
        Must be run successfully before an I2C sequence starts

        Args:
            id (int): one-hot mutex ID to claim with (slowcontrol=1, RMM Microblaze=2, RMM firmware=4)
            poll_delay (float): delay between attempts (default=1.0 seconds)
            poll_attempts_timeout (int): number of attempts to make before giving up (default=None)
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)
        
        """
        self.dbg_log.info("Attempting to claim I2C Mutex")
        mutex_claimed = False
        mutex_poll_attempts = 0
        
        # while we don't have the mutex claimed
        while(not mutex_claimed):
            # write our ID to the I2C_REQ
            success = self.regWrite("I2C_REQ", id, reg_tag="C", pre_success=pre_success)
            
            # read the I2C_GNT register to see if we have claimed the mutex
            success, grant_id = self.regRead("I2C_GNT", reg_tag="C", pre_success=success)

            if grant_id ==  ("0x" + RMMUtils.hexFormat(id)):
                mutex_claimed = True
                break       # break here to avoid a compulsory `poll_delay` between each I2C transaction
            mutex_poll_attempts = mutex_poll_attempts + 1
            self.dbg_log.info("Attempts to claim I2C Mutex = " + str(mutex_poll_attempts))
            
            # optional timeout feature
            if poll_attempts_timeout is not None:
                if mutex_poll_attempts > poll_attempts_timeout:
                    # Ask the user if we want to force clear the mutex
                    self.dbg_log.error("Failed to claim I2C Mutex before timeout occured!")
                    return False

            # optional delay feature
            if poll_delay:
                time.sleep(poll_delay)

        self.dbg_log.info("Successfully claimed Mutex!")
        return success


    def i2cReleaseMutex(self, pre_success=True):
        """Releases the I2C Mutex so another device can access it
        
        Must be run after an I2C sequence is complete

        Args:
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)

        """

        success = self.regWrite("I2C_REQ", self.I2C_MUTEX_IDLE_ID, "C", pre_success=pre_success)
        self.dbg_log.info("Released Mutex!")
        return success



    def i2cWriteByte(self, i2c_addr, data, pre_success=True):
        """Writes a single byte to an I2C address

        Useful for setting I2C muxes

        Args:
            i2c_addr (hex/int/str): I2C address of target
            data (hex/int/str): data byte to write
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)
        
        Returns:
            success (bool)
        """
        success = self.regWrite("I2C_ADDR", i2c_addr, "C", pre_success=pre_success)
        success = self.regWrite("I2C_CMD", self.I2C_CMD_WRITE_ONE_BYTE, "C", pre_success=success)
        success = self.regWrite("I2C_REG_ADDR", data, "C", pre_success=success)    # as we are only writing one byte, it goes here
        return self.__i2cSendCmdAndWaitUntilDone(pre_success = success)

    def i2cWrite8bReg(self, i2c_addr, reg_addr, data, pre_success=True):
        """Writes to an 8bit register at an I2C address

        Args:
            i2c_addr (hex/int/str): I2C address of target
            reg_addr (hex/int/str): target register address inside I2C device
            wdata (hex/int/str): data byte to write
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)
        
        Returns:
            success (bool)
        """
        success = self.regWrite("I2C_ADDR", i2c_addr, "C", pre_success=pre_success)
        success = self.regWrite("I2C_CMD", self.I2C_CMD_WRITE_TWO_BYTES, "C", pre_success=success)
        success = self.regWrite("I2C_REG_ADDR", reg_addr, "C", pre_success = success)
        success = self.regWrite("I2C_WDATA", data, "C", pre_success=success)
        return self.__i2cSendCmdAndWaitUntilDone(pre_success = success)

    def i2cWrite16bReg(self, i2c_addr, reg_addr, data, pre_success=True):
        """Writes to a 16bit register at an I2C address

        Args:
            i2c_addr (hex/int/str): I2C address of target
            reg_addr (hex/int/str): target register address inside I2C device
            wdata (hex/int/str): data bytes to write
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)
        
        Returns:
            success (bool)
        """
        success = self.regWrite("I2C_ADDR", i2c_addr, "C", pre_success=pre_success)
        success = self.regWrite("I2C_CMD", self.I2C_CMD_WRITE_THREE_BYTES, "C", pre_success=success)
        success = self.regWrite("I2C_REG_ADDR", reg_addr, "C", pre_success=success)
        success = self.regWrite("I2C_WDATA", data, "C", pre_success=success)
        return self.__i2cSendCmdAndWaitUntilDone(pre_success = success)

    def i2cRead8bReg(self, i2c_addr, reg_addr, pre_success=True):
        """Reads from an 8bit register at an I2C address

        Args:
            i2c_addr (hex/int/str): I2C address of target
            reg_addr (hex/int/str): target register address inside I2C device
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)
            data (str): read data
        """
        success = self.regWrite("I2C_ADDR", i2c_addr, "C", pre_success=pre_success)
        success = self.regWrite("I2C_CMD", self.I2C_CMD_READ_BYTE, "C", pre_success=success)
        success = self.regWrite("I2C_REG_ADDR", reg_addr, "C", pre_success=success)
        success = self.__i2cSendCmdAndWaitUntilDone(pre_success=success)
        success, data = self.regRead("I2C_RDATA", "C", pre_success=success)
        return success, data

    def i2cRead16bReg(self, i2c_addr, reg_addr, pre_success=True):
        """Reads from an 16bit register at an I2C address

        Args:
            i2c_addr (hex/int/str): I2C address of target
            reg_addr (hex/int/str): target register address inside I2C device
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)
            data (str): read data
        """
        success = self.regWrite("I2C_ADDR", i2c_addr, "C", pre_success=pre_success)
        success = self.regWrite("I2C_CMD", self.I2C_CMD_READ_TWO_BYTES, "C", pre_success=success)
        success = self.regWrite("I2C_REG_ADDR", reg_addr, "C", pre_success=success)
        success = self.__i2cSendCmdAndWaitUntilDone(pre_success=success)
        success, data = self.regRead("I2C_RDATA", "C", pre_success=success)
        return success, data


    def i2cRead8bRegSi570(self, i2c_addr, reg_addr, pre_success=True):
        """Reads from an 8bit register at an I2C address (Si570 only)

        Args:
            i2c_addr (hex/int/str): I2C address of target Si570
            reg_addr (hex/int/str): target register address inside I2C device
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)
            data (str): read data
        """

        success = self.regWrite("I2C_ADDR", i2c_addr, "C", pre_success=pre_success)
        success = self.regWrite("I2C_CMD", self.I2C_CMD_READ_BYTE_SI570, "C", pre_success=success)
        success = self.regWrite("I2C_REG_ADDR", reg_addr, "C", pre_success=success)
        success = self.__i2cSendCmdAndWaitUntilDone(pre_success=success)
        success, data = self.regRead("I2C_RDATA", "C", pre_success=success)
        return success, data

    def i2cDelay(self, delay_us, pre_success=True):
        """Use the I2C controller to time a delay
        
        Args:
            delay_us (int): Delay in microseconds
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)
        """
        success = self.regWrite("I2C_CMD", self.I2C_CMD_DELAY, "C", pre_success=pre_success)
        success = self.regWrite("I2C_WDATA", delay_us, "C", pre_success=success)
        return self.__i2cSendCmdAndWaitUntilDone(pre_success=success)
        
    def __i2cSendCmdAndWaitUntilDone(self, pre_success=True):
        """Starts the I2C controller state machine and waits for it to complete

        Args:
            pre_success (bool): Optional, for chaining functions together and getting a combined success value (default=True)

        Returns:
            success (bool)
        """
        success = self.regWrite("I2C_START", 0, "C", pre_success=pre_success)  # generate rising edge
        success = self.regWrite("I2C_START", 1, "C", pre_success=success)
        exp_value = "0x" + RMMUtils.hexFormat(1)
        data = "0x" +  RMMUtils.hexFormat(0)    # ensure we always execute the register read once
        while data != exp_value:
            success, data = self.regRead("I2C_DONE", "C", pre_success=success)
            time.sleep(0.01)
        return success