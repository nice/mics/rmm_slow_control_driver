import logging
import signal
import os

"""A simple wrapper around the python logging functionality for some ease of use

Usage: 
    Call setup_logger() with the appropriate arguments to get a logger

Other util functions:
    handle_error() to force quit the currently running python, with an error message
    which_bits_are_set() takes a hex string and returns a list of indexes of the bits which are high
    try_open() to attempt to open a file

"""

LOGGING_FORMAT = logging.Formatter("%(asctime)s %(filename)s, %(lineno)d %(levelname)s %(message)s")
CONSOLE_LOGGING_FORMAT = logging.Formatter("%(levelname)s: %(message)s")


def __setupConsoleLogging(log_level = logging.INFO):
    consolehandler = logging.StreamHandler()
    consolehandler.setFormatter(CONSOLE_LOGGING_FORMAT)
    consolehandler.setLevel(log_level)
    return consolehandler

def setupLogger(name, log_fname, log_mode = 'a', log_level = logging.DEBUG, \
                mirror_to_console = 'y', console_log_level = logging.INFO \
    ): 
    """
    Sets up a new Logger

    Args:
        name (str) : Name of logger to create
        log_fname (Path): Filepath to log file
        log_mode (str) : Logging Mode (either 'a' for append (default) or 'w' for overwrite)
        log_level (int) : Loglevel to log to file (default=logging.DEBUG)
        mirror_to_console (str) : 'y' to also log to console (default='y') 
        console_log_level (int) : Loglevel to log to console (default=logging.INFO)

    Returns:
        logger (Logger): A configured python logger

    """  

    logger = logging.getLogger(name)
    if logger:
        [logger.removeHandler(handler) for handler in logger.handlers[:]]
    logger.setLevel(log_level)          
    handler = logging.FileHandler(log_fname, mode = log_mode)           
    handler.setFormatter(LOGGING_FORMAT)
    handler.setLevel(log_level)
    logger.addHandler(handler)
    if mirror_to_console == 'y':
        consolehandler = __setupConsoleLogging(log_level = console_log_level)
        logger.addHandler(consolehandler)
    return logger    

def __forceQuit(msg = 0):
  """A 'belt and braces' quit. 
  sys.exit() alone can be insufficent to terminate python called on thread from shell-script"""
  os.kill(os.getpid(), signal.SIGTERM) 
  

def handleError(message = "An error occurred", askUserForKeyPressBeforeQuitting = True, logger=None):
    """On error, either quit immediately, or pause to ask User to press a key quit """
    if logger == None:
        print("ERROR: " + message)
    else:
        logger.error(message)

    if not askUserForKeyPressBeforeQuitting :
        __forceQuit();
    else :
        print("Paused due to Error. Press a key to contine, q to quit ")   
        if (input() == 'q'):
            logger.info("user chose to quit")
            __forceQuit()
        else:
            logger.info("user chose to continue despite error")
            return

def whichBitsAreSet(data):
    """ Takes a hex string and returns a list of indexes containing set bits"""
    indexes = []
    data = int(data, 16)    
    for i in range(12):
        mask = 1 << i;   
        if data & mask:   # if non-zero, bit is set
            indexes.append(i)
    return indexes

def tryOpen(fname, wr = ''):
    try:
        fp = open(fname, wr)
    except IOError:
        handleError(message = "Unable to open file " + fname, askUserForKeyPressBeforeQuitting = True)   
    return fp 

def hexFormat(dat):
    """Converts a value to a 32b hex string with no '0x' prefix
    (for use in reg_write/reg_read)
    
    Examples:
        "0x16" => '00000016'
        0x16   => '00000016'
        "16"   => '00000010'
        16     => '00000010'
    
    Args:
        dat (str/int): value to convert

    Returns:
        string(str): converted value ready for use in RMMRegisterAccess.regWrite() etc

    """
    
    return "{:08x}".format(int(str(dat), 0))